<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="uudiste-list.php">Uudised</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ümbrikupalga saajate arv...</li>
                        </ol>
                    </nav>

                    <h1 class="page-title">
                        Accordion
                    </h1>

                    <div class="v-accordion">
                        <div class="button-group text-right v-accordion__btn-group">
                            <button class="btn btn-primary v-btn-primary v-btn-primary--small" type="button" data-toggle="collapse" data-target=".multi-collapse:not(.show)" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Ava Kõik</button>
                            <button class="btn btn-primary v-btn-primary v-btn-primary--small" type="button" data-toggle="collapse" data-target=".multi-collapse.show" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Sulge kõik</button>
                        </div>
                        <div class="v-accordion__item">
                            <a class="btn btn-primary v-btn-primary v-accordion__btn collapsed" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="v-accordion__btn-icon"></i>Euroopa Ühenduse tolliseadustiku artikkel 201 lg 3 sätestab, et võlgnik on deklarant ja kaudse esindamise korral on isik, kelle eest tollideklaratsioon esitatakse samuti võlgnik. Mida tähendab solidaarne nõue, kui deklaratsioonil on valitud tasumisviisiks 3 (saaja maksab)?</a>
                            <div class="collapse multi-collapse" id="multiCollapseExample1">
                                <div class="v-accordion__collapse-wrap">
                                    <p>Juhul, kui tollideklaratsioonil valitakse tasumisviisiks 3 (saaja maksab), kajastub põhivõlg kauba saajal. Deklarandil kajastub summa solidaarse nõudena. Kui põhivõlgnikul raha ei ole, hakatakse solidaarseid võlgu solidaarse isiku ettemaksukontolt tasuma peale kahe päeva möödumist maksetähtpäevast.</p>
                                </div>
                            </div>
                        </div>
                        <div class="v-accordion__item">
                            <a class="btn btn-primary v-btn-primary v-accordion__btn collapsed" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="v-accordion__btn-icon"></i>Kes väljastab arvedeklaratsioone ja kui pikk on nende kehtivusaeg?</a>
                            <div class="collapse multi-collapse" id="multiCollapseExample2">
                                <div class="v-accordion__collapse-wrap">
                                    <p>Arvedeklaratsioone väljastab eksportija. Alla 6000 euro maksvale kaubale väljastatavaks arvedeklaratsiooniks on kindla sõnastusega allkirjastatud tekst kauba arvel või muul lisadokumendil. Kui kauba maksumus on üle 6000 euro, tuleb eksportijal taotleda Maksu- ja Tolliametist (edaspidi MTA) kinnitatud eksportija õigus (antakse kindla kombinatsiooniga number, mille eksportija märgib arvedeklaratsioonil määratud kohta). Kui eksportijal sellist õigust taotletud ei ole, saab ta oma kaubale taotleda tolli poolt kinnitatud päritolusertifikaadi (EUR1).</p>
                                    <p>Arvedeklaratsioonide kehtivusaeg sõltub lepingutest. Üldjuhul on kahe- ja mitmepoolsete lepingute korral kehtivusajaks 4 kuud (välja arvatud mõningad erandid), aga näiteks GSP riikidele on see 10 kuud. Kui kaubad paigutatakse näiteks tollilattu või kui neile rakendatakse mõnda muud peatamismenetlust, siis kauba vabasse ringlusse deklareerimisel aktsepteeritakse päritolutõendit kuni 2 aastat alates selle väljastamisest.</p>
                                </div>
                            </div>
                        </div>
                        <div class="v-accordion__item">
                            <a class="btn btn-primary v-btn-primary v-accordion__btn collapsed" data-toggle="collapse" href="#multiCollapseExample3" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="v-accordion__btn-icon"></i>Eesti ettevõte soovib rentida Venemaa ettevõttele tööstusliku seadme. Kas selline tehing on eksport, müük või ajutine väljaviimine, kui periood ei ületa 3 aastat? Kui rendiperiood ületab 3 aastat (ajutise väljaviimise tähtaega), siis kas seda perioodi on võimalik pikendada ja selle aja möödudes teha reimport? Juhul, kui nimetatud seade amortiseerub jääkväärtuseni, siis millal on tagasivedu Eestisse majanduslikult mitteotstarbekas, kas reimport on siiski kohustuslik protseduur? </a>
                            <div class="collapse multi-collapse" id="multiCollapseExample3">
                                <div class="v-accordion__collapse-wrap">
                                    <p>Mainitud tehing on ühenduse kauba ajutine väljaviimine, kavatsusega kaup kolme aasta jooksul reimportida. Ekspordi tolliprotseduuri koodiks märgitakse tollideklaratsioonile 23 00. Ajutise väljaviimise tähtaega (3 aastat) pikendada ei ole võimalik. Teatud juhtudel, erandolukordades, võib toll anda maksuvabastuse ka tagasitoodavale kaubale, mille väljaveo tähtaeg on ületatud. Erandolukorraks ei saa lugeda lepingu pikendamist. Reimport ei ole kohustuslik protseduur, ajutiselt väljaviidud kaubale võib järgneda müügitehing, mis kajastatakse tehingule järgneval perioodil käibedeklaratsioonis ekspordina. Väljaveo tõenduseks on eelnevalt vormistatud ajutise väljaveo tollideklaratsioon. Ajutiselt väljaviidud kaubale järgnevast müügitehingust tolli teavitama ei pea. Kui kaup tuuakse tagasi Eestisse peale ajutise väljaveo tähtaja (3 aasta) möödumist, siis ei ole võimalik rakendada maksuvabastust, vaid kaup maksustatakse nagu kolmanda riigi kaup.</p>
                                </div>
                            </div>
                        </div>
                        <div class="v-accordion__item">
                            <a class="btn btn-primary v-btn-primary v-accordion__btn collapsed" data-toggle="collapse" href="#multiCollapseExample4" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="v-accordion__btn-icon"></i>Mida tähendab ühenduse tollistaatusega kaup?</a>
                            <div class="collapse multi-collapse" id="multiCollapseExample4">
                                <div class="v-accordion__collapse-wrap">
                                    <p>Juhul, kui tollideklaratsioonil valitakse tasumisviisiks 3 (saaja maksab), kajastub põhivõlg kauba saajal. Deklarandil kajastub summa solidaarse nõudena. Kui põhivõlgnikul raha ei ole, hakatakse solidaarseid võlgu solidaarse isiku ettemaksukontolt tasuma peale kahe päeva möödumist maksetähtpäevast.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>