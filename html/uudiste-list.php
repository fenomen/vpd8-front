<?php require('components/header.php'); ?>

<main role="main" class="v-main">
    <div class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 ">
    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="#">Uudised</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kõik uudised</li>
                        </ol>
                    </nav>
    
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card v-main-card v-main-card--bg-grey">
                            
                                <div class="d-lg-none">
                                    <a class="v-filter-collapse" data-toggle="collapse" href="#filterCollapse" role="button" aria-expanded="false" aria-controls="filterCollapse">
                                        <span class="v-filter-collapse__label">Otsi uudiseid</span><i class="v-filter-collapse__icon fas fa-angle-down"></i>
                                    </a>
                                </div>
    
                                <form action="" class="v-filter collapse d-lg-block" id="filterCollapse">
                                    <div class="form-group">
                                        <label for="search">Otsing</label>
    
                                        <input type="text" class="form-control form-control-sm mb-2" id="search" aria-describedby="news search">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Sisaldab kõiki sõnu
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                            <label class="form-check-label" for="exampleRadios2">
                                                Täpne fraas
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
                                            <label class="form-check-label" for="exampleRadios3">
                                                Sisaldab suvalist sõna
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Kuupäev</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios11" value="option11" checked>
                                            <label class="form-check-label" for="exampleRadios11">
                                                Kõik
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios21" value="option21">
                                            <label class="form-check-label" for="exampleRadios21">
                                                Viimased 7 päeva
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios31" value="option31">
                                            <label class="form-check-label" for="exampleRadios31">
                                                Viimased 30 päeva
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios41" value="option41">
                                            <label class="form-check-label" for="exampleRadios41">
                                                Viimased 12 kuud
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios51" value="option51">
                                            <div class="form-row align-items-center">
                                                <div class="date-min col">
                                                    <input type="text" class="form-control form-control-sm datepicker" id="inlineFormInputGroup">
                                                </div>
                                                <div class="col-auto">-</div>
                                                <div class="date-max col">
                                                    <input type="text" class="form-control form-control-sm datepicker" id="inlineFormInputGroup">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Teema</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios12" value="option12" checked>
                                            <label class="form-check-label" for="exampleRadios12">
                                                Maksu- ja tolliamet
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios22" value="option22">
                                            <label class="form-check-label" for="exampleRadios22">
                                                Maksud
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios32" value="option32">
                                            <label class="form-check-label" for="exampleRadios32">
                                                Aktsiisid
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios42" value="option42">
                                            <label class="form-check-label" for="exampleRadios42">
                                                Maksukontroll
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios52" value="option52">
                                            <label class="form-check-label" for="exampleRadios52">
                                                Tollikontroll
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios62" value="option62">
                                            <label class="form-check-label" for="exampleRadios62">
                                                Salakaubandus
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios72" value="option72">
                                            <label class="form-check-label" for="exampleRadios72">
                                                Maksuõigusrikkumised
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios82" value="option82">
                                            <label class="form-check-label" for="exampleRadios82">
                                                Deklaratsioonid
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="exampleRadios" id="exampleRadios92" value="option92">
                                            <label class="form-check-label" for="exampleRadios92">
                                                E-maksuamet/e-toll
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="search">Sektsioon</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios13" value="option13" checked>
                                            <label class="form-check-label" for="exampleRadios13">
                                                Kõik
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios23" value="option23">
                                            <label class="form-check-label" for="exampleRadios23">
                                                Eraklient
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios33" value="option33">
                                            <label class="form-check-label" for="exampleRadios33">
                                                Äriklient
                                            </label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-block btn-primary v-btn-primary">Otsi</button>
                                </form>
                                
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <h1 class="page-title">Uudised</h1>
                            <div class="v-news-list">
                                <div class="v-news-list__item">
                                    <img src="http://via.placeholder.com/150x100" alt="" class="v-news-list__image">
                                    <div class="v-news-list__date">27.02.2018</div>
                                    <div class="v-news-list__title"><a href="uudis.php">Aab: Viljandi on omaette fenomen</a></div>
                                    <div class="v-news-list__content">
                                        <p>Put light against light - you have nothing. Put dark against dark - you have nothing. It's the contrast of light and dark that each give the other one meaning.</p>
                                    </div>
                                </div>
                                <div class="v-news-list__item">
                                    <div class="v-news-list__date">27.02.2018</div>
                                    <div class="v-news-list__title"><a href="uudis.php">Riigi 2017. aasta tulud ületasid kulusid</a></div>
                                    <div class="v-news-list__content">
                                        <p>It's so important to do something every day that will make you happy. Fluff it up a little and hypnotize it. God gave you this gift of imagination. Use it. When you buy that first tube of paint it gives you an artist license. A tree cannot be straight if it has a crooked trunk.</p>
                                    </div>
                                </div>
                                <div class="v-news-list__item">
                                    <img src="http://via.placeholder.com/150x100" alt="" class="v-news-list__image">
                                    <div class="v-news-list__date">27.02.2018</div>
                                    <div class="v-news-list__title"><a href="uudis.php">Valitsus kiitis heaks elektriaktsiisi soodustuse energiamahukatele ettevõtetele</a></div>
                                    <div class="v-news-list__content">
                                        <p>There isn't a rule. You just practice and find out which way works best for you. You create the dream - then you bring it into your world. So often we avoid running water, and running water is a lot of fun. Everything is happy if you choose to make it that way.</p>
                                    </div>
                                </div>
                                <div class="v-news-list__item">
                                    <div class="v-news-list__date">27.02.2018</div>
                                    <div class="v-news-list__title"><a href="uudis.php">Aab: Viljandi on omaette fenomen</a></div>
                                    <div class="v-news-list__content">
                                        <p>Put light against light - you have nothing. Put dark against dark - you have nothing. It's the contrast of light and dark that each give the other one meaning.</p>
                                    </div>
                                </div>
                                <div class="v-news-list__item">
                                    <img src="http://via.placeholder.com/150x100" alt="" class="v-news-list__image">
                                    <div class="v-news-list__date">27.02.2018</div>
                                    <div class="v-news-list__title"><a href="uudis.php">Riigi 2017. aasta tulud ületasid kulusid</a></div>
                                    <div class="v-news-list__content">
                                        <p>It's so important to do something every day that will make you happy. Fluff it up a little and hypnotize it. God gave you this gift of imagination. Use it. When you buy that first tube of paint it gives you an artist license. A tree cannot be straight if it has a crooked trunk.</p>
                                    </div>
                                </div>
                                <div class="v-news-list__item">
                                    <img src="http://via.placeholder.com/150x100" alt="" class="v-news-list__image">
                                    <div class="v-news-list__date">27.02.2018</div>
                                    <div class="v-news-list__title"><a href="uudis.php">Valitsus kiitis heaks elektriaktsiisi soodustuse energiamahukatele ettevõtetele</a></div>
                                    <div class="v-news-list__content">
                                        <p>There isn't a rule. You just practice and find out which way works best for you. You create the dream - then you bring it into your world. So often we avoid running water, and running water is a lot of fun. Everything is happy if you choose to make it that way.</p>
                                    </div>
                                </div>
    
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination pagination-sm">
                                        <li class="page-item">
                                        <a class="page-link" href="javascript:;" aria-label="Previous">
                                            <span aria-hidden="true"><i class="fas fa-angle-left"></i></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                        <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                        <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                        <li class="page-item">
                                        <a class="page-link" href="javascript:;" aria-label="Next">
                                            <span aria-hidden="true"><i class="fas fa-angle-right"></i></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                        </li>
                                    </ul>
                                </nav>
    
                            </div>
                        </div>
                    </div>
    
                </div>
            </div>
        </div>
    </div>
</main>

<?php require('components/footer.php'); ?>