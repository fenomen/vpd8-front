<?php require('components/header.php'); ?>

<main role="main" class="v-main">
    <div class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="#">Tulu deklareerimine</a></li>
                            <li class="breadcrumb-item"><a href="#">Maksustatavad tulud</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Internetis kaupade tellimisel...</li>
                        </ol>
                    </nav>
    
                    <h1 class="page-title">Internetis kaupade tellimisel ja müümisel tekkiv maksukohustus</h1>
    
                    <article class="v-article">
                        
                        <div class="v-article__content">
                            <p>Kaupade müümine interneti vahendusel on kasvav trend üle maailma. Suur osa kaupadest müüakse selleks, et leida
                                oma kasutatud asjadele uus omanik. Samuti on hulk inimesi, kes välismaistest portaalidest tellivad uut kaupa,
                                et see mõningase juurdehindlusega edasi müüa ning seeläbi tulu teenida. Maksu- ja Tolliameti tähelepanu all
                                on mainitud tegevustest aga üldjuhul ainult viimane.</p>
    
                            <p>Maksustamisel ei erine veebis kaupade müümine kuidagi poes näiteks riiete müümisest. Mõlemad on käsitletavad ettevõtlusena,
                                kui teenitakse maksustatavat tulu, tegutsetakse järjepidevalt ning tegeletakse enda tegevuse reklaamimisega. Seega ei ole
                                sisulist vahet, millises kanalis (osta.ee, buduaar.ee, facebook.com jt) tegutsetakse, vaid millega täpsemalt tegeletakse.</p>
    
                            <p>Kui isik tegeleb kaupade müügiga ettevõtluse raames, peab ta olema kantud äriregistrisse. Äriregistrisse kantud 
                                füüsilisest isikust ettevõtja (FIE) võib kaubandustegevusega seotud kulud ettevõtlustulust maha arvata ning saadud
                                kasum maksustatakse tulu- ja sotsiaalmaksuga ning kohustusliku kogumispensioni süsteemiga liitunud isiku puhul ka
                                kohustusliku kogumispensioni maksega.</p>
    
                            <p>Olukorras, kui eraisikule on esemete müük juhuslik tegevus ning tegevus ei vasta ettevõtluse tunnustele, tuleb kaupade 
                                müügist saadud tulu deklareerida kord aastas esitatava residendist füüsilise isiku tuludeklaratsiooniga. Maksustatakse 
                                saadud kasu, st ostu- ja müügihinna vahe, mida on vähendatud ostu- ja müügiga seotud tehingukuludega.</p>
    
                            <p>E-kaubanduse kontrolle viiakse läbi süstemaatiliselt ning amet monitoorib netikauplejaid riskipõhiselt – seda nii Facebookis
                                kui ka spetsiaalselt kauba müügile keskendunud lehtedel. Teeme koostööd Eesti E-kaubanduse Liiduga, Tarbijakaitseametiga
                                ning Politsei- ja Piirivalveametiga.</p>
    
                            <p>Palju tuleb ka vihjeid inimestelt, kes on märganud internetis kahtlast kauplejat või on ise mõne sellisega kokku puutunud.
                                Samuti tuleb vihjeid isikute kohta, kes jätavad oma kohustused ostja ees täitmata. <a href="javascript:;">Täpsemalt vihjete andmisest</a></p>
    
                            <p>Järgnevalt selgitame, millistel juhtudel tekib internetis asjade tellimisel ja müümisel maksukohustus ning milliseid makse 
                                tuleb maksukohustuse tekkimisel tasuda.</p>
    
                            <ul class="v-links-list">
                                <li class="v-links-list__item"><a href="javascript:;">Maksukohustus kaupade tellimisel internetist</a></li>
                                <li class="v-links-list__item"><a href="javascript:;">Maksukohustus kaupade müümisel internetis</a></li>
                            </ul>
    
                            <div class="v-article__grey-section">
                                <h4>Veebiseminari "Internetikaubanduse võlud ja valud" salvestus</h4>
    
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/1qNRvuHli-M" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
    
                                <p>&nbsp;</p>
                                
                                <div class="v-info-combo">
                                    <a href="javascript:;" target="_blank">Kuidas rajada usaldusväärne e-pood?</a>
                                    <p>Kätlin Aren, Tarbijakaitseamet (minutid 35:10-1:05:51)</p>
                                </div>
                                <div class="v-info-combo">
                                    <a href="javascript:;" target="_blank">Kohustuslik lepingueelne teave ja 14-päevane taganemisõigus</a>
                                    <p>Signe Kõiv, Eesti E-kaubanduse Liit (minutid 00:16-35:09)</p>
                                </div>
                                <div class="v-info-combo">
                                    <a href="javascript:;" target="_blank">Internetiäri maksustamine</a>
                                    <p>Priit Vao, Maksu- ja Tolliamet (minutid 1:05:50-1:32:59)</p>
                                </div>
                                <div class="v-info-combo">
                                    <a href="javascript:;" target="_blank">Ettevõtlustulu lihtsustatud maksustamine - ettevõtluskonto</a>
                                    <p>Eha kütt, Maksu- ja Tolliamet (minutid 1:32:59-2:02:17)</p>
                                </div>
                            </div>
    
                            <div class="v-article__grey-section">
                                <h4>Seotud failid</h4>
                                <ul class="v-links-list">
                                    <li class="v-links-list__item"><a href="javascript:;">2017_internetis_kaupdade_müümisel_tekkiv_maksukohustus.pdf</a></li>
                                    <li class="v-links-list__item"><a href="javascript:;">2018_internetis_kaupdade_müümisel_tekkiv_maksukohustus.pdf</a></li>
                                    <li class="v-links-list__item"><a href="javascript:;">Faili nime saab asendada faili pealkiri väljaga</a></li>
                                </ul>
                            </div>
    
                            <div class="v-article__date">Viimati uuendatud 06.07.2018</div>
    
                        </div>
                    </article>
    
                </div>
            </div>
        </div>
    </div>
</main>

<?php require('components/footer.php'); ?>