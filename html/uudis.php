<?php require('components/header.php'); ?>

<main role="main" class="v-main">
    <div class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="uudiste-list.php">Uudised</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ümbrikupalga saajate arv...</li>
                        </ol>
                    </nav>
    
                    <div class="row">
                        <div class="col-lg-8">
                            <h1 class="page-title">Ümbrikupalga saajate arv suurenes 13 protsendini</h1>
                            <article class="v-article">
                                
                                <div class="v-article__content">
                                    <div class="v-article__image">
                                        <img src="http://via.placeholder.com/480x360" alt="Uudist illustreeriv pilt." class="img-fluid">
                                    </div>
                                    <p><strong>Täna avaldatud Eesti konjunktuuriinstituudi (EKI) varimajanduse uuringu tulemuste järgi kasvas ümbrikupalga saajate arv kaheksalt protsendilt 13 protsendile. Illegaalse alkoholi tarbimine on jäänud samale tasemele, kuid kasvanud on nende inimeste hulk, kes tarbivad salasigarette vahetevahel.</strong></p>
    
                                    <p>Maksu- ja tolliameti (MTA) teabeosakonna juhataja <strong>Janek Leisi</strong> sõnul on positiivne, et kasvanud on palgasaajate osakaal, kes ei poolda ümbrikupalka. Võrreldes eelmise uuringuga kasvas mittepooldajate osakaal 62 protsendilt 70 protsendini.</p>
    
                                    <p>„Uuring toetab ameti kontrollitegevusel põhinevat hinnangut, et täielikult ebaseaduslikku töötasu saavate inimeste hulk jätkuvalt väheneb, kuid osalist ümbrikupalka saavate inimeste hulk on kasvanud,“ ütles Leis. Tulemused näitavad, et surve ümbrikupalkade maksmiseks on tulnud eeskätt tööandjatelt, mida võib tõlgendada tugeva palgasurvega. „Kuigi rohkem on neid inimesi, kes saavad osaliselt ümbrikupalka, on siiski positiivne see, et illegaalselt saadud summad on jäänud väiksemaks. Seega ümbrikupalka makstakse väiksemates summades ja suurema osa tulust teenivad inimesed ausalt,“ lisas Leis.</p>
    
                                    <p>Illegaalseid alkohoolseid jooke tarbis uuringu järgi teadlikult 5% elanikest. Sellele eelnenud, 2016. aastal tarbis salaalkoholi teadlikult 3% alkoholitarbijatest. „Salaalkoholi tarbimine on jätkuvalt pigem ebapopulaarne, kuid näeme siiski ohtusid Lätist ostetud alkoholi edasimüümises. Samas ei ole MTA kontrollid toitlustus- ja meelelahutuskohtades seni märkimisväärseid koguseid Läti alkoholi tuvastanud,“ rõhutas Leis.</p>
    
                                    <p>Teadlikult ostis 2016. aastal salasigarette 16% suitsetajatest ja 2017. aastal 26% suitsetajatest. Janek Leisi sõnul illegaalsete ja legaalsete sigarettide hinnaerinevus samas vähenes, millest võib järeldada, et kuigi salasigarettide eelistamine on kasvanud, on nende kättesaadavus muutunud halvemaks. „Üheks põhjuseks on siin kindlasti ka MTA edukas kontrollitegevus ning tänu läbivalgustusseadmete lisandumisele kasvanud avastamise võimekus,“ lisas Leis.</p>
    
                                    <p>Varimajanduse uuring tugineb 2017. aasta lõpus täiskasvanud elanikkonna seas toimunud küsitluste tulemustele, millele vastas 1074 inimest vanuses 18-74 eluaastat. EKI on illegaalse alkoholi ja sigarettide tarbimise ning ümbrikupalkade maksmise uuringuid teinud alates 1998. aastast.</p>
    
                                    <h4>Olulisemad näitajad</h4>
    
                                    <h5><strong>Ümbrikupalk</strong></h5>
    
                                    <ul><li>Ümbrikupalka sai 2017. aastal 13% töötajatest, mis on võrreldes 2016. aastaga 5% rohkem.</li>
                                        <li>Illegaalne töötasu moodustas ümbrikupalka saanud inimeste sissetulekutest keskmiselt 31% (2016. aastal 44%).</li>
                                        <li>Kõige enam oli mustalt töötajaid noorte (18-29aastaste) ja madalama haridusega inimeste hulgas.</li>
                                        <li>Ettevõtlusvaldkondadest on illegaalse töötasu maksmine kõige enam levinud kaubanduses (34% kõigist varjatud töötasu saanud inimestest), sellele järgnesid transpordivaldkond (23%) ja toitlustus (12%).</li>
                                        <li>Ümbrikupalga pooldajate arv jäi samaks (13%) ja seda ei pooldanud 70% palgasaajatest (2016. aastal 62%).</li>
                                        <li>Ümbrikupalga maksmisega ei olnud 2017. aastal rahul 52%, aasta varem 37% illegaalse töötasu saajatest. Ümbrikupalgaga rahuolematuse kõige olulisemaks põhjuseks on sotsiaalsete garantiide nõrgenemine, aga ka ebaõiglus ausate maksumaksjate ja ettevõtjate suhtes ning tööandja vastu seadusliku kaitse puudumine.</li>
                                        <li>Maksudena jäi 2017. aastal hinnanguliselt saamata 149 miljonit eurot, 2016. aastal hinnanguliselt 109 miljonit eurot.</li>
                                    </ul>
                                    
                                    <h5><strong>Salaalkohol</strong></h5>
    
                                    <ul><li>Illegaalseid alkohoolseid jooke tarbis uuringu järgi teadlikult 5% elanikest. Sellele eelnenud, 2016. aastal tarbis salaalkoholi teadlikult 3% alkoholitarbijatest.</li>
                                        <li>Peamiselt osteti illegaalset alkoholi odavama hinna tõttu.</li>
                                        <li>Salaalkoholi ostjaid teadsid sagedamini nooremad inimesed, mehed, eestlased ja samuti madalama haridustasemega inimesed.</li>
                                        <li>Kõige rohkem elas salaalkoholi ostjaid teadvaid inimesi Lääne-Eestis ja Lõuna-Eestis.</li>
                                        <li>Kokku tarbiti Eesti Konjunktuuriinstituudi arvutuste kohaselt 2017. aastal 2,2 miljonit liitrit salaalkoholi, mis sisaldab ka Lätist müügiks toodud alkoholikoguseid (0,9 miljonit liitrit).</li>
                                        <li>Kogu Eesti elanike tarbitud viinast moodustas salaalkohol hinnanguliselt 20-23%.</li>
                                        <li>Illegaalse alkoholituru tõttu jäi riigil aktsiisi- ja käibemaksu näol saamata ligikaudu 15,6 miljonit eurot.</li>
                                    </ul>
                                    
                                    <h5><strong>Salasigaretid</strong></h5>
    
                                    <ul><li>Salasigarette ostis 2017. aastal teadlikult 26% suitsetajatest, aasta varem oli neid 16%.</li>
                                        <li>Illegaalseid sigarette ostis 2017. aastal pidevalt 4%, vahetevahel 13% ja väga harva 9% suitsetajatest. Võrreldes eelnenud aastaga kasvas oluliselt vahetevahel salasigarette ostnud suitsetajate osakaal.</li>
                                        <li>Legaalne sigaretipakk maksis 2017. aastal keskmiselt 3,54 eurot (2016. aastal 3.23 eurot), salasigarettide paki keskmine hind oli samal ajal 2,22 eurot (2016. aastal 1,92 eurot).</li>
                                        <li>Kogu sigarettide siseturumahust moodustasid salasigaretid hinnanguliselt 25-31%.</li>
                                        <li>Riigil jäi illegaalse sigaretikaubanduse tõttu maksudena saamata ligikaudu 76,8 miljonit eurot aktsiisi- ja käibemaksu.</li>
                                    </ul>
                                </div>
                                <div class="v-article__date">Lisatud 06.07.2018</div>
                            </article>
                        </div>
                        <div class="col-lg-4">
                            <div class="card v-main-card v-main-card--bg-grey">
                                <h5>Autor</h5>
                                <div class="v-info-combo">
                                    <div class="v-info-combo__image"><img src="http://via.placeholder.com/120x120" alt=""></div>
                                    <div class="v-info-combo__lead">Uku Tampere</div>
                                    <p>kommunikatsiooniosakond</p>
                                    <p>pressiesindaja</p>
                                    <p>56880216</p>
                                    <p><a href="javascript:;">uku.tampere@emta.ee</a></p>
                                </div>
                            </div>
                            <div class="card v-main-card v-main-card--bg-grey">
                                <h5>Teemad</h5>
                                <ul class="v-links-list">
                                    <li class="v-links-list__item"><a href="javascript:;">Maksud</a></li>
                                    <li class="v-links-list__item"><a href="javascript:;">Aktsiisid</a></li>
                                    <li class="v-links-list__item"><a href="javascript:;">Salakaubandus</a></li>
                                    <li class="v-links-list__item"><a href="javascript:;">Maksuõigusrikkumised</a></li>
                                </ul>
                            </div>
                            <div class="card v-main-card v-main-card--bg-grey">
                                <h5>Failid</h5>
                                <ul class="v-links-list">
                                    <li class="v-links-list__item"><a href="javascript:;">varimajanduse_uuring_2017.pdf</a></li>
                                </ul>
                            </div>
                            <div class="card v-main-card v-main-card--bg-grey">
                                <h5>Seotud sündmus</h5>
                                <p class="v-main-card__date">15. veebruar</p>
                                <h6>Algab füüsilise isiku 2018 aasta tuludeklaratsiooni elektrooniline esitamine</h6>
                                <p><small>Tuludeklaratsiooni saab esitada Maksu- ja Tolliametile alates 16. veebruarist kuni 2. aprillini 2018. Tuludeklaratsiooni esitavad residendist
                                    füüsilised isikud (edaspidi isikud) eelmise kalendriaasta tulu kohta. Tuludeklaratsiooni saab esitada Maksu ja Tolliametile
                                    alates 15. veebruarist kuni 2. aprillini 2018.</small>
                                </p>
                                <p><small><strong>Toimumise koht</strong>: Üle eesti</small></p>
                                <a href="javascript:;"><small>Seotud link, mis viib täpselt sinna, kuhu haldur soovib</small></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php require('components/footer.php'); ?>