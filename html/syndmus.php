<?php require('components/header.php'); ?>

<main role="main" class="v-main">
    <div class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="uudiste-list.php">Sündmused</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Tulu- ja sotsiaalmaksu ning...</li>
                        </ol>
                    </nav>
    
                    <div class="row">
                        <div class="col-lg-8">
                            <h1 class="page-title">Tulu- ja sotsiaalmaksu ning kohustusliku kogumispensioni ja töötuskindlustuse maksete deklaratsiooni vorm TSD esitamine. Tulumaksu, sotsiaalmaksu, töötuskindlustusmakse ja kogumispensioni makse ülekandmine</h1>
                            <article class="v-article">
                                <div class="v-article__info">
                                    <div>
                                        <strong>Toimumise aeg:</strong> <span>Teisipäev, 10.04.2018 13:00</span> - <span>13.04.2018 00:00</span>
                                    </div>
                                    <div>
                                        <strong>Toimumise koht:</strong> Üle eesti
                                    </div>
                                </div>
                                <div class="v-article__content">
                                    <p>Tulumaksu kinnipidaja esitab vormi TSD töölepingu alusel töötavate töötajate või teenistussuhtes olevate ametnike kohta, kelle töötamine ei ole töötamise registri andmetel peatatud.</p>
                                    <p>Esitab ka Eestis püsivat tegevuskohta omav või tööandjana tegutsev mitteresident, kes teeb füüsilisele isikule või mitteresidendile maksustatavaid väljamakseid.</p>
                                    <a href="javascript:;" target="_blank">TSD »</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-lg-4 v-sidebar v-sidebar--right">
                            <div class="card v-main-card v-main-card--bg-grey">
                                <h5>Autor</h5>
                                <div class="v-info-combo">
                                    <div class="v-info-combo__image"><img src="http://via.placeholder.com/120x120" alt=""></div>
                                    <div class="v-info-combo__lead">Uku Tampere</div>
                                    <p>kommunikatsiooniosakond</p>
                                    <p>pressiesindaja</p>
                                    <p>56880216</p>
                                    <p><a href="javascript:;">uku.tampere@emta.ee</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php require('components/footer.php'); ?>