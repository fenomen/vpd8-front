<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <!-- columni klassid muudetud ja lisatud mt-4 ja mb-5 -->
                <div class="col-md-8 col-lg-4 offset-md-2 offset-lg-4 mt-4 mb-5">

                    <h1 class="page-title">
                        Lähtesta oma parool
                    </h1>

                    <!-- Drupal tabs block -->
                    <div id="block-tabs" class="settings-tray-editable block block-core block-local-tasks-block" data-drupal-settingstray="editable">
                        <nav class="mb-4" role="navigation" aria-label="Sakid">
                            <h2 class="visually-hidden">Peasakid</h2>
                            <ul class="nav nav-tabs primary">
                                <li class="nav-item">
                                    <a href="user.php" class="nav-link" data-drupal-link-system-path="user/login">Sisene</a>
                                </li>
                                <li class="nav-item is-active">
                                    <a href="password.php" class="nav-link active is-active" data-drupal-link-system-path="user/password">Lähtesta oma parool<span class="visually-hidden">(aktiivne sakk)</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="v-user">
                        <!-- Drupal login-form markup -->
                        <form class="user-pass" data-drupal-selector="user-pass" action="/user/password" method="post" id="user-pass" accept-charset="UTF-8">
                            <div class="js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-name form-item-name">
                                <label for="edit-name" class="js-form-required form-required">Kasutajanimi või e-posti aadress</label>
                                <input autocorrect="off" autocapitalize="off" spellcheck="false" autofocus="autofocus" data-drupal-selector="edit-name" type="text" id="edit-name" name="name" value="" size="60" maxlength="254" class="form-text required form-control" required="required" aria-required="true">
                            </div>
                            <!-- P tag-i asemel SMALL tag -->
                            <small>Parooli lähtestamise juhend saadetakse sinu e-posti aadressile.</small>
                            <input autocomplete="off" data-drupal-selector="form-i5jnadpzngchl59hcdjajqa9hbqi9bpddseppaboq2s" type="hidden" name="form_build_id" value="form-I5JNAdPZNGchL59HcdjAjqa9HbQI9BpdDsEpPaBOQ2s">
                            <input data-drupal-selector="edit-user-pass" type="hidden" name="form_id" value="user_pass">
                            <div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper" id="edit-actions">
                                <input data-drupal-selector="edit-submit" type="submit" id="edit-submit" name="op" value="Sisesta" class="button js-form-submit form-submit btn btn-primary v-btn-primary mt-3">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>