<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="uudiste-list.php">Uudised</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ümbrikupalga saajate arv...</li>
                        </ol>
                    </nav>

                    <h1 class="page-title">
                        Veebivormid
                    </h1>

                    <div class="v-webform">
                        <form>
                            <div class="form-group v-form-group">
                                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example textinput *" required="required">
                                <label for="formGroupExampleInput" class="v-form-group__label v-form-group__label--float">Example textinput *</label>
                            </div>
                            <div class="form-group v-form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Example textarea"></textarea>
                                <label for="exampleFormControlTextarea1" class="v-form-group__label v-form-group__label--float">Example textarea</label>
                            </div>
                            <div class="form-group v-form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Example email *" required="required">
                                <label for="exampleInputEmail1" class="v-form-group__label v-form-group__label--float">Example email *</label>
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group v-form-group">
                                <label for="exampleFormControlSelect1" class="v-form-group__label">Example select</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group v-form-group">
                                <label for="exampleFormControlSelect2" class="v-form-group__label">Example multiple select</label>
                                <select multiple class="form-control" id="exampleFormControlSelect2">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group v-form-group">
                                <label for="" class="v-form-group__label">Example checkbox</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        Default checkbox
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" disabled>
                                    <label class="form-check-label" for="defaultCheck2">
                                        Disabled checkbox
                                    </label>
                                </div>
                            </div>
                            <div class="form-group v-form-group">
                                <label for="" class="v-form-group__label">Example inline checkbox: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                    <label class="form-check-label" for="inlineCheckbox1">1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                    <label class="form-check-label" for="inlineCheckbox2">2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" disabled>
                                    <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
                                </div>
                            </div>
                            <div class="form-group v-form-group">
                                <label for="" class="v-form-group__label">Example radios</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        Default radio
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">
                                        Second default radio
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
                                    <label class="form-check-label" for="exampleRadios3">
                                        Disabled radio
                                    </label>
                                </div>
                            </div>
                            <div class="form-group v-form-group">
                                <label for="" class="v-form-group__label">Example inline radios: </label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
                                    <label class="form-check-label" for="inlineRadio3">3 (disabled)</label>
                                </div>
                            </div>
                        </form>

                        <h2>Veebivormi näide</h2>

                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="v-webform">
                                    <form action="">
                                        <div class="form-group v-form-group">
                                            <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="Eesnimi *" required="required">
                                            <label for="formGroupExampleInput1" class="v-form-group__label v-form-group__label--float">Eesnimi *</label>
                                        </div>
                                        <div class="form-group v-form-group">
                                            <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Perekonnanimi *" required="required">
                                            <label for="formGroupExampleInput2" class="v-form-group__label v-form-group__label--float">Perekonnanimi *</label>
                                        </div>
                                        <div class="form-group v-form-group">
                                            <input type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="E-posti aadress *" required="required">
                                            <label for="exampleInputEmail2" class="v-form-group__label v-form-group__label--float">E-posti aadress *</label>
                                            <small id="emailHelp" class="form-text text-muted">Me ei jaga teie e-posti aadressit kolmandatele osapooltele</small>
                                        </div>
                                        <div class="form-group v-form-group">
                                            <input type="text" class="form-control" id="formGroupExampleInput3" placeholder="Telefon">
                                            <label for="formGroupExampleInput3" class="v-form-group__label v-form-group__label--float">Telefon</label>
                                        </div>
                                        <div class="form-group v-form-group">
                                            <label for="exampleFormControlSelect3" class="v-form-group__label">Pöördumise teema *</label>
                                            <select class="form-control" id="exampleFormControlSelect3">
                                                <option>- Vali -</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                                <option>Lorem ipsum</option>
                                            </select>
                                        </div>
                                        <div class="form-group v-form-group">
                                            <textarea class="form-control" id="exampleFormControlTextarea2" rows="6" placeholder="Kirjeldus"></textarea>
                                            <label for="exampleFormControlTextarea2" class="v-form-group__label v-form-group__label--float">Kirjeldus</label>
                                        </div>
                                        <button class="btn btn-primary v-btn-primary">Saada</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>