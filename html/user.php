<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <!-- columni klassid muudetud ja lisatud mt-4 ja mb-5 -->
                <div class="col-md-8 col-lg-4 offset-md-2 offset-lg-4 mt-4 mb-5">

                    <h1 class="page-title">
                        Sisene
                    </h1>

                    <!-- Drupal tabs block -->
                    <div id="block-tabs" class="settings-tray-editable block block-core block-local-tasks-block" data-drupal-settingstray="editable">
                        <nav class="mb-4" role="navigation" aria-label="Sakid">
                            <h2 class="visually-hidden">Peasakid</h2>
                            <ul class="nav nav-tabs primary">
                                <li class="nav-item is-active">
                                    <a href="user.php" class="nav-link active is-active" data-drupal-link-system-path="user/login">Sisene<span class="visually-hidden">(aktiivne sakk)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="password.php" class="nav-link" data-drupal-link-system-path="user/password">Lähtesta oma parool</a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="v-user">
                        <!-- Drupal login-form markup -->
                        <form class="user-login-form" data-drupal-selector="user-login-form" action="/user/login" method="post" id="user-login-form" accept-charset="UTF-8">
                            <!-- Esimesele välja containerile lisatud mb-3 -->
                            <div class="js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-name form-item-name mb-3">
                                <label for="edit-name" class="js-form-required form-required">Kasutajanimi</label>
                                <input autocorrect="none" autocapitalize="none" spellcheck="false" autofocus="autofocus" data-drupal-selector="edit-name" aria-describedby="edit-name--description" type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required form-control" required="required" aria-required="true">

                                <!-- DIV muudetud SMALL-iks -->
                                <small id="edit-name--description" class="description form-text text-muted">
                                    Sisesta oma VP kasutajanimi.
                                </small>
                            </div>
                            <div class="js-form-item form-item js-form-type-password form-type-password js-form-item-pass form-item-pass">
                                <label for="edit-pass" class="js-form-required form-required">Parool</label>
                                <input data-drupal-selector="edit-pass" aria-describedby="edit-pass--description" type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required form-control" required="required" aria-required="true">

                                <!-- DIV muudetud SMALL-iks -->
                                <small id="edit-pass--description" class="description form-text text-muted">
                                    Sisesta kasutajanimele vastav salasõna.
                                </small>
                            </div>
                            <input autocomplete="off" data-drupal-selector="form-j9-yl0upgdawfq8iohritn5iathfc6xjh12j7iakpem" type="hidden" name="form_build_id" value="form-J9_YL0UPGdaWfQ8iOhrITN5IAThFC6xjh12J7iAKpEM">
                            <input data-drupal-selector="edit-user-login-form" type="hidden" name="form_id" value="user_login_form">
                            <div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper" id="edit-actions">
                                <!-- nupule lisatud v-btn-primary klass ja mt-3 klass -->
                                <input data-drupal-selector="edit-submit" type="submit" id="edit-submit" name="op" value="Sisene" class="button js-form-submit form-submit btn btn-primary v-btn-primary mt-3">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>