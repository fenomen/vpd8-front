<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<title>Valitsusportaal</title>

	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" media="all">
    <link rel='stylesheet' href='assets/css/main.css' type='text/css' media='all' />
	<style type="text/css">
		body {
			position: relative;
		}
		.main {
			width: 100%;
			max-width: 1450px;
			margin: 0 auto;
			padding: 100px 0 100px 200px;
		}

		/* Header */
		.header {
			overflow: hidden;
			background-color: #fff;
			border-bottom: 1px #ddd solid;
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			z-index: 999;
		}
		.header .logo {
			float: left;
		}
		.header .logo a {
			display: block;
			width: 150px;
			height: 59px;
			font-size: 0;
			background: url(img/logo.svg) no-repeat 50%;
			background-size: auto 26px;
		}

		/* Nav */
		.nav {
			width: 300px;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 999;
		}
		.v-categories {
			margin-left: 0;
		}
		.nav a {
			display: block;
			padding: 10px 20px;
			color: #fff;
			text-transform: uppercase;
			background-color: #0078d2;
			border-bottom: 1px #085277 solid;
			position: relative;
		}
		.nav .active a,
		.nav a:hover {
			text-decoration: none;
			color: #00a0dc;
			background-color: #fff;
		}
		.nav .active a:before {
			content: "";
			display: block;
			height: 4px;
			background-color: #00a0dc;
			position: absolute;
			left: 0;
			right: 0;
			top: 0;
		}

		/* Content */
		.section {
			margin-bottom: 100px;
		}
		.section-title,
		.section-sub-title {
			margin-bottom: 40px;
			font-family: 'Open Sans', Arial, Helvetica, sans-serif;
			font-size: 30px;
			line-height: 1;
			position: relative;
		}
		.section-title {
			padding: 15px 20px 15px 55px;
			color: #fff;
			background-color: #0078d2;
			border-radius: 2px;
		}
		.section-sub-title {
			margin-top: 60px;
			padding-left: 55px;
			font-size: 24px;
		}
		.section-title span,
		.section-sub-title span {
			font-family: Arial, Helvetica, Sans-serif;
			font-size: 16px;
			line-height: 1;
			position: absolute;
			top: 50%;
			transform: translateY(-50%);
		}
		.section-title span {
			width: 28px;
			height: 28px;
			line-height: 28px;
			text-align: center;
			color: #0078d2;
			background-color: #fff;
			border-radius: 50%;
			left: 15px;
		}
		.section-sub-title span {
			padding: 5px 10px;
			color: #fff;
			background-color: #0078d2;
			border-radius: 2px;
			left: 0;
		}
		.block-headings h1,
		.block-headings h2,
		.block-headings h3,
		.block-headings h4 {
			margin-bottom: 25px;
		}
		.o-card__wrap {
			margin-bottom: 0;
		}
		.alert,
		pre,
		.section-block,
		.block-buttons .o-btn-group__inline {
			margin-top: 30px !important;
			margin-bottom: 30px !important;
		}
		.v-page-header {
			padding: 0;
		}
	</style>
</head>
<body>
    
<div class="main">

	<div class="section" id="block-headings">
		<h2 class="section-title"><span class="count"></span>Titles</h2>
		<div class="content block-headings">
			<h1>H1 header - 36px</h1>
			<h2>H2 header - 26px</h2>
			<h3>H3 header - 20px</h3>
			<p><a href="#"><a href="#">link teksti sees</a> cum soluta nobis eleifend option <a href="#">link teksti sees.</a></a></p>
		</div>
	</div>
	<div class="section" id="block-headings">
		<h2 class="section-title"><span class="count"></span>Breadcrumbs</h2>
		<ul class="v-breadcrumbs">
			<li class="v-breadcrumbs__link"><a href="#">Avaleht</a></li>
			<li>Coop Pank</li>
			<li>Lisaks</li>
			<li>Uudised</li>
		</ul>
		<pre class="line-numbers">
				<code class="language-markup">
				<?php
					$html = '
					<ul class="v-breadcrumbs">
						<li class="v-breadcrumbs__link"><a href="#">Avaleht</a></li>
						<li>Coop Pank</li>
						<li>Lisaks</li>
						<li>Uudised</li>
					</ul>
					';
					echo str_replace("<","&lt;", $html);
				?>
				</code>
			</pre>
	</div>
	
	<div class="section" id="block-buttons">
		<h2 class="section-title"><span class="count"></span>Buttons</h2>
		<div class="content block-buttons">

			<div class="o-btn-group__inline">
				<p><a href="#" class="btn btn-primary">Primary</a></p>
				<p><a href="#" class="btn btn-secondary">Secondary</a></p>
				<p><a href="#" class="btn btn-third">Third</a></p>
			</div>
			<pre class="line-numbers">
				<code class="language-markup">
				<?php
					$html = '
					<p><a href="#" class="btn btn-primary">Primary</a></p>
					<p><a href="#" class="btn btn-secondary">Secondary</a></p>
					<p><a href="#" class="btn btn-third">Third</a></p>
					';
					echo str_replace("<","&lt;", $html);
				?>
				</code>
			</pre>

			<h3 class="section-sub-title"><span>2.1</span>Button sizes</h3>

			<div class="o-btn-group__inline">
				<p><a href="#" class="btn btn-primary btn-sm">Small</a></p>
				<p><a href="#" class="btn btn-secondary btn-sm">Small</a></p>
				<p><a href="#" class="btn btn-third btn-sm">Small</a></p>
				<p><a href="#" class="btn btn-blank btn-sm">Small</a></p>
			</div>
			<pre class="line-numbers">
				<code class="language-markup">
				<?php
					$html = '
					<p><a href="#" class="btn btn-primary btn-sm">Small</a></p>
					<p><a href="#" class="btn btn-secondary btn-sm">Small</a></p>
					<p><a href="#" class="btn btn-third btn-sm">Small</a></p>
					<p><a href="#" class="btn btn-blank btn-sm">Small</a></p>
					';
					echo str_replace("<","&lt;", $html);
				?>
				</code>
			</pre>

			<h3 class="section-sub-title">Buttons inline group</h3>

			<div class="o-btn-group__inline">
				<p><a href="#" class="btn btn-primary">Button</a></p>
				<p><a href="#" class="btn btn-primary">Button</a></p>
				<p><a href="#" class="btn btn-primary">Button</a></p>
			</div>
			<pre class="line-numbers">
				<code class="language-markup">
				<?php
					$html = '
					<div class="o-btn-group__inline">
						<p><a href="#" class="btn btn-primary">Button</a></p>
						<p><a href="#" class="btn btn-primary">Button</a></p>
						<p><a href="#" class="btn btn-primary">Button</a></p>
					</div>
					';
					echo str_replace("<","&lt;", $html);
				?>
				</code>
			</pre>

		</div>
	</div>
</div>

<script type="text/javascript">
/**
 * Copyright (c) 2007-2015 Ariel Flesler - aflesler ○ gmail • com | http://flesler.blogspot.com
 * Licensed under MIT
 * @author Ariel Flesler
 * @version 2.1.3
 */
;(function(f){"use strict";"function"===typeof define&&define.amd?define(["jquery"],f):"undefined"!==typeof module&&module.exports?module.exports=f(require("jquery")):f(jQuery)})(function($){"use strict";function n(a){return!a.nodeName||-1!==$.inArray(a.nodeName.toLowerCase(),["iframe","#document","html","body"])}function h(a){return $.isFunction(a)||$.isPlainObject(a)?a:{top:a,left:a}}var p=$.scrollTo=function(a,d,b){return $(window).scrollTo(a,d,b)};p.defaults={axis:"xy",duration:0,limit:!0};$.fn.scrollTo=function(a,d,b){"object"=== typeof d&&(b=d,d=0);"function"===typeof b&&(b={onAfter:b});"max"===a&&(a=9E9);b=$.extend({},p.defaults,b);d=d||b.duration;var u=b.queue&&1<b.axis.length;u&&(d/=2);b.offset=h(b.offset);b.over=h(b.over);return this.each(function(){function k(a){var k=$.extend({},b,{queue:!0,duration:d,complete:a&&function(){a.call(q,e,b)}});r.animate(f,k)}if(null!==a){var l=n(this),q=l?this.contentWindow||window:this,r=$(q),e=a,f={},t;switch(typeof e){case "number":case "string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)){e= h(e);break}e=l?$(e):$(e,q);case "object":if(e.length===0)return;if(e.is||e.style)t=(e=$(e)).offset()}var v=$.isFunction(b.offset)&&b.offset(q,e)||b.offset;$.each(b.axis.split(""),function(a,c){var d="x"===c?"Left":"Top",m=d.toLowerCase(),g="scroll"+d,h=r[g](),n=p.max(q,c);t?(f[g]=t[m]+(l?0:h-r.offset()[m]),b.margin&&(f[g]-=parseInt(e.css("margin"+d),10)||0,f[g]-=parseInt(e.css("border"+d+"Width"),10)||0),f[g]+=v[m]||0,b.over[m]&&(f[g]+=e["x"===c?"width":"height"]()*b.over[m])):(d=e[m],f[g]=d.slice&& "%"===d.slice(-1)?parseFloat(d)/100*n:d);b.limit&&/^\d+$/.test(f[g])&&(f[g]=0>=f[g]?0:Math.min(f[g],n));!a&&1<b.axis.length&&(h===f[g]?f={}:u&&(k(b.onAfterFirst),f={}))});k(b.onAfter)}})};p.max=function(a,d){var b="x"===d?"Width":"Height",h="scroll"+b;if(!n(a))return a[h]-$(a)[b.toLowerCase()]();var b="client"+b,k=a.ownerDocument||a.document,l=k.documentElement,k=k.body;return Math.max(l[h],k[h])-Math.min(l[b],k[b])};$.Tween.propHooks.scrollLeft=$.Tween.propHooks.scrollTop={get:function(a){return $(a.elem)[a.prop]()}, set:function(a){var d=this.get(a);if(a.options.interrupt&&a._last&&a._last!==d)return $(a.elem).stop();var b=Math.round(a.now);d!==b&&($(a.elem)[a.prop](b),a._last=this.get(a))}};return p});


jQuery(function ($) {
	'use strict';
	$(document).on('click', '.nav a', function(event) {
		event.preventDefault();
		$.scrollTo( $(this).attr('href'), 400, {offset: -100});
	});

	var nav = ( function() {

		var $el = $('.nav'),
			items =  [];

		function init() {

		}

		function addItem(item) {
			var $item = $(item),
				id = $(item).attr('id'),
				title = $(item).find('.section-title').text();

			$el.append('<li><a href="#'+ id +'">'+ title +'</a></li>')
		}

		return {
			addItem: addItem
		}

	})();

	var $sections = $('.section');

	$sections.each( function(i, el) {
		nav.addItem(el);
		$(el).find('.count').text(i + 1);
	});

});
</script>
</body>
</html>