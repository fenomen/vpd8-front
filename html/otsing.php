<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
                    <h1 class="page-title">
                        Vasted otsingule "Eesti"
                    </h1>

                    <div class="v-search">
                        <div class="v-search__header">
                            <form class="form-inline">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-lg">
                                    <button type="submit" class="btn btn-primary v-btn-primary btn-lg">Otsing</button>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Sisaldab kõiki sõnu
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Täpne fraas
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
                                        <label class="form-check-label" for="exampleRadios3">
                                            Sisaldab suvalist sõna
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="v-search__category">
                            <ul class="nav">
                                <li class="v-search__category--active"><a href="javascript:;">Kõik (34)</a></li>
                                <li><a href="javascript:;">Uudised (24)</a></li>
                                <li><a href="javascript:;">Sündmused (2)</a></li>
                                <li><a href="javascript:;">Artiklid (10)</a></li>
                            </ul>
                        </div>

                        <div class="v-search__list">
                            <!-- item 1 start -->
                            <div class="v-search__list-item">
                                <span class="badge badge-primary">Uudised</span>
                                <h6 class="v-search__teaser-title"><a href="uudis.php">Teenuse käibe tekkimise koha määramine, käibemaksumäär ning deklareerimine</a><span class="v-search__teaser-date">25.01.2018</span></h6>
                                <p class="v-search__teaser-content">
                                    ... Teenuse osutamine <strong>Eesti</strong> käibemaksukohustuslase poolt Teenuse osutamisel teise liikmesriigi piiratud maksukohustuslastele tuleb jälgida samu reegleid nagu teenuse osutamisel tavapärasele teise liikmesriigi maksukohustuslasele. Kui ...
                                </p>
                            </div>
                            <!-- item 2 start -->
                            <div class="v-search__list-item">
                                <span class="badge badge-primary">Sündmused</span>
                                <h6 class="v-search__teaser-title"><a href="syndmus.php">Kauba käibe maksustamine ja deklareerimine</a><span class="v-search__teaser-date">25.01.2018</span></h6>
                                <p class="v-search__teaser-content">
                                    ... alati nullmääraga maksustavate kaupdade loetelu 15 lõikes 3 ja alati maksuvabade kaupade loetelu -s 16. Kauba käibe tekkimise koht on <strong>Eesti</strong>, kui: Kaup toimetatakse saajale või tehakse talle mul viisil kättesaadavaks <strong>Eesti</strong>s, eksporditakse <strong>Eesti</strong>st , teostatakse kauba ühendusesisest käivet või kaugmüüki <strong>Eesti</strong>st teise ...
                                </p>
                            </div>
                            <!-- item 3 start -->
                            <div class="v-search__list-item">
                                <span class="badge badge-primary">Artiklid</span>
                                <h6 class="v-search__teaser-title"><a href="artikkel.php">Teenuste maksustamine</a><span class="v-search__teaser-date">25.01.2018</span></h6>
                                <p class="v-search__teaser-content">
                                    ... SISUKORD 5.1. Teenuse mõiste 5.2. Teenuse käibe tekkimise koht 5.2.1. Teenused, mille osutamisel on käibe tekkimise koht <strong>Eesti</strong> ja mis maksustatakse <strong>Eesti</strong>s 5.2.2. Teenused, mille osutamisel käibe tekkimise koht ei ole <strong>Eesti</strong> 5.3. Kinnisasjaga seotud teenused 5.4. Kultuuri- ...
                                </p>
                            </div>
                        </div>

                        <nav aria-label="Page navigation example">
                            <ul class="pagination pagination-sm">
                                <li class="page-item">
                                <a class="page-link" href="javascript:;" aria-label="Previous">
                                    <span aria-hidden="true"><i class="fas fa-angle-left"></i></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="javascript:;">1</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                <li class="page-item">
                                <a class="page-link" href="javascript:;" aria-label="Next">
                                    <span aria-hidden="true"><i class="fas fa-angle-right"></i></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                </li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>