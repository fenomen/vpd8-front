<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sisukaart</li>
                        </ol>
                    </nav>

                    <h1 class="page-title">
                        Sisukaart
                    </h1>

                    <ul class="v-sitemap">
                        <li>
                            <a href="javascript:;">Eraklient</a>
                            <ul>
                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                <li><a href="javascript:;">Maksumäärad</a></li>
                                <li>
                                    <a href="javascript:;">Deklaratsioonide vormid ja nende täitmise juhised</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta</a></li>
                                        <li><a href="javascript:;">2016. aasta</a></li>
                                        <li><a href="javascript:;">2015. aasta</a></li>
                                        <li><a href="javascript:;">2014. aasta</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                <li>
                                    <a href="javascript:;">Maksumäärad</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                        <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                        <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                        <li>
                                            <a href="javascript:;">Maksumäärad</a>
                                            <ul>
                                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                                <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                                <li><a href="javascript:;">Maksumäärad</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                <li><a href="javascript:;">Maksumäärad</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">Äriklient</a>
                            <ul>
                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                <li>
                                    <a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta</a></li>
                                        <li><a href="javascript:;">2016. aasta</a></li>
                                        <li><a href="javascript:;">2015. aasta</a></li>
                                        <li><a href="javascript:;">2014. aasta</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                <li>
                                    <a href="javascript:;">Maksumäärad</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                        <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                        <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                        <li>
                                            <a href="javascript:;">Maksumäärad</a>
                                            <ul>
                                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                                <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                                <li><a href="javascript:;">Maksumäärad</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;">Deklaratsioonide vormid ja nende täitmise juhised</a>
                                </li>
                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                <li>
                                    <a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta</a></li>
                                        <li><a href="javascript:;">2016. aasta</a></li>
                                        <li><a href="javascript:;">2015. aasta</a></li>
                                        <li><a href="javascript:;">2014. aasta</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                <li>
                                    <a href="javascript:;">Maksumäärad</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                        <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                        <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                        <li><a href="javascript:;">Maksumäärad</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;">Deklaratsioonide vormid ja nende täitmise juhised</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">Kontaktid ja ametist</a>
                            <ul>
                                <li><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
                                <li><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018. aasta tulude deklareerimisel</a></li>
                                <li><a href="javascript:;" target="_blank">Tulumaksu tagastamine ja juurdemakse</a></li>
                                <li><a href="javascript:;">Maksumäärad</a></li>
                                <li>
                                    <a href="javascript:;">Deklaratsioonide vormid ja nende täitmise juhised</a>
                                    <ul>
                                        <li><a href="javascript:;">2017. aasta</a></li>
                                        <li><a href="javascript:;">2016. aasta</a></li>
                                        <li><a href="javascript:;">2015. aasta</a></li>
                                        <li><a href="javascript:;">2014. aasta</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>