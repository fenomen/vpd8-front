<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<title>Valitsusportaal</title>

	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
	<style type="text/css">
		html, body {
			font-family: 'Open Sans', Arial, Helvetica, sans-serif;
			font-size: 16px;
			color: #222;
		}
		a, a:hover, a:focus {
			color: #015cae;
			text-decoration: none;
			outline: none;
		}
		a:hover {
			text-decoration: underline;
		}
		h1,h2,h3,h4,p,ul,li {
			margin: 0;
			padding: 0;
		}
		ul {
			list-style: none;
		}
		h1,h2 {
			font-family: 'Open Sans', Arial, Helvetica, sans-serif;
			font-weight: 400;
		}
		.container {
			width: 100%;
			max-width: 800px;
			margin: 0 auto;
		}
		.header {
			overflow: hidden;
			background-color: #fff;
            padding: 30px 0;
		}
		.header .logo {
			float: left;
		}
		.content {
			padding: 20px 0;
		}
		.content h2 {
			margin: 60px 0 20px;
			font-weight: 600;
		}
		.list li {
			padding: 10px 15px;
			margin-bottom: 1rem;
			background-color: #f5f5f5;
			border-bottom: 1px #fff solid;
		}
		.list h3 {
			margin-bottom: 5px;
			font-size: 20px;
			font-weight: 600;
		}
		.list p {
			font-size: 14px;
		}
		.list p a {
			color: #222;
		}
	</style>
</head>
<body>

<div class="header container">
	<p class="logo"><a href="index.php"><img src="img/vpd8_logo.png" /></a></p>
</div>

<div class="container">
	<div class="content">
		<ul class="list">
			<li>
				<h3><a href="components.php">Komponendid</a></h3>
			</li>
			<li>
				<h3><a href="avaleht.php">Avaleht</a></h3>
			</li>
			<li>
				<h3><a href="artikkel.php">Artikkel</a></h3>
			</li>
			<li>
				<h3><a href="uudis.php">Uudis</a></h3>
			</li>
			<li>
				<h3><a href="uudiste-list.php">Uudiste list</a></h3>
			</li>
			<li>
				<h3><a href="syndmus.php">Sündmus</a></h3>
			</li>
			<li>
				<h3><a href="syndmuste-kalender.php">Sündmuste kalender</a></h3>
			</li>
			<li>
				<h3><a href="otsing.php">Otsing</a></h3>
			</li>
			<li>
				<h3><a href="sisukaart.php">Sisukaart</a></h3>
			</li>
			<li>
				<h3><a href="veebivormid.php">Veebivormid</a></h3>
			</li>
			<li>
				<h3><a href="user.php">Sisselogimise vaade</a></h3>
			</li>
			<li>
				<h3><a href="password.php">Parooli lähtestamise vaade</a></h3>
			</li>
			<li>
				<h3><a href="403.php">403</a></h3>
			</li>
			<li>
				<h3><a href="404.php">404</a></h3>
			</li>
			<li>
				<h3><a href="vahelehed.php">Vahelehed (tabs)</a></h3>
			</li>
			<li>
				<h3><a href="accordion.php">KKK (accordion)</a></h3>
			</li>
		</ul>
	</div>
</div>

</body>
</html>
