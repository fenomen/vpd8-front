<?php require('components/header.php'); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="uudiste-list.php">Sündmused</a></li>
                        </ol>
                    </nav>

                    <div class="row">
                        <div class="col-12">
                            <h1 class="page-title">Sündmuste kalender</h1>

                            <div class="v-event-calendar">

                                <div class="v-event-calendar__head">
                                    <a href="javascript:;"><< Eelmine</a>
                                    <span class="v-event-calendar__month">August</span>
                                    <a href="javascript:;">Järgmine >></a>
                                </div>

                                <div class="v-event-calendar__filter">
                                    <a class="v-event-calendar__filter-collapse-link collapsed" href="#v-event-calendar__filter-collapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="v-event-calendar__filter-collapse">Otsi sündmuseid <i class="vpicon vpicon__down"></i></a>
                                    <form class="form-inline collapse" id="v-event-calendar__filter-collapse">
                                        <div class="form-group">
                                            <label for="inlineFormCustomSelectPref">Sündmuse tüüp</label>
                                            <select class="custom-select col-auto" id="inlineFormCustomSelectPref">
                                                <option selected>Kõik</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="inlineFormCustomSelectPref">Sektsioon</label>
                                            <select class="custom-select col-auto" id="inlineFormCustomSelectPref">
                                                <option selected>Kõik</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="inlineFormCustomSelectPref">Kirjeid lehel</label>
                                            <select class="custom-select col-auto" id="inlineFormCustomSelectPref">
                                                <option selected>20</option>
                                                <option value="1">50</option>
                                                <option value="2">100</option>
                                                <option value="3">200</option>
                                            </select>
                                        </div>
                                        
                                        <div class="flex-item text-right">
                                            <button type="button" class="btn btn-primary btn-sm v-btn-primary v-btn-primary--active">Kuu</button>
                                            <button type="button" class="btn btn-primary btn-sm v-btn-primary">Aasta</button>
                                        </div>
                                        
                                    </form>
                                </div>

                                <div class="v-event-calendar-list">
                                    <!--First item start-->
                                    <div class="v-event-calendar-list__item row">
                                        <div class="col-lg-2">
                                            <a href="javascript:;" class="v-event-calendar-list__date">
                                                <i class="vpicon vpicon__down"></i>
                                                <div>17.veebruar - 22.veebruar</div>
                                            </a>
                                        </div>
                                        <div class="col-lg-10">
                                            <h6 class="v-event-calendar-list__title"><a href="syndmus.php">Tulu- ja sotsiaalmaksu ning kohustusliku kogumispensioni ja töötuskindlustuse maksete deklaratsiooni vorm TSD esitamine. Tulumaksu, sotsiaalmaksu, töötuskindlustusmakse ja kogumispensioni makse ülekandmine</a></h6>
                                        </div>
                                        <div class="col-lg-10 order-lg-last">
                                            <div class="v-event-calendar-list__teaser">
                                                <p>Diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea <a href="javasciprt:;" target="blank">takimata sanctus</a> est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est <a href="javascript:;">Lorem ipsum</a> dolor sit amet.</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="v-event-calendar-list__info">
                                                <p>
                                                    <strong>Toimumise aeg:</strong> 17.02.2018 11:00 - 22.02.2018 22:00
                                                </p>
                                                <p>
                                                    <strong>Toimumise koht:</strong> Üle Eesti
                                                </p>
                                                <p>
                                                    <strong>Loe lisaks:</strong>
                                                    <ul class="v-links-list">
                                                        <li class="v-links-list__item"><a href="javascript:;">Mingi link</a></li>
                                                        <li class="v-links-list__item"><a href="javascript:;">Lorem ipsum dolorem</a></li>
                                                        <li class="v-links-list__item"><a href="javascript:;">Ipsum dem amet</a></li>
                                                    </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--First item end-->
                                    <!--Second item start-->
                                    <div class="v-event-calendar-list__item row">
                                        <div class="col-lg-2">
                                            <a href="javascript:;" class="v-event-calendar-list__date">
                                                <i class="vpicon vpicon__down"></i>
                                                <div>17.veebruar</div>
                                            </a>
                                        </div>
                                        <div class="col-lg-10">
                                            <h6 class="v-event-calendar-list__title"><a href="syndmus.php">Laeva kütusega varustamise aruande esitamine</a></h6>
                                        </div>
                                        <div class="col-lg-10 order-lg-last">
                                            <div class="v-event-calendar-list__teaser">
                                                <p>Diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea <a href="javasciprt:;" target="blank">takimata sanctus</a> est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est <a href="javascript:;">Lorem ipsum</a> dolor sit amet.</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="v-event-calendar-list__info">
                                                <p>
                                                    <strong>Toimumise aeg:</strong> 17.02.2018 11:00
                                                </p>
                                                <p>
                                                    <strong>Toimumise koht:</strong> Üle Eesti
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Second item end-->
                                    <!--Third item start-->
                                    <div class="v-event-calendar-list__item row">
                                        <div class="col-lg-2">
                                            <a href="javascript:;" class="v-event-calendar-list__date">
                                                <i class="vpicon vpicon__down"></i>
                                                <div>18.veebruar</div>
                                            </a>
                                        </div>
                                        <div class="col-lg-10">
                                            <h6 class="v-event-calendar-list__title"><a href="syndmus.php">Hasartmängumaksu deklaratsiooni lisade 1–3 ja 6–8 esitamine eelmise maksustamisperioodi kohta ning hasartmängumaksu tasumine</a></h6>
                                        </div>
                                        <div class="col-lg-10 order-lg-last">
                                            <div class="v-event-calendar-list__teaser">
                                                <p>Diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea <a href="javasciprt:;" target="blank">takimata sanctus</a> est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est <a href="javascript:;">Lorem ipsum</a> dolor sit amet.</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="v-event-calendar-list__info">
                                                <p>
                                                    <strong>Toimumise aeg:</strong> 17.02.2018 11:00
                                                </p>
                                                <p>
                                                    <strong>Toimumise koht:</strong> Üle Eesti
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Third item end-->
                                    <!--Fourth item start-->
                                    <div class="v-event-calendar-list__item row">
                                        <div class="col-lg-2">
                                            <a href="javascript:;" class="v-event-calendar-list__date">
                                                <i class="vpicon vpicon__down"></i>
                                                <div>19.veebruar</div>
                                            </a>
                                        </div>
                                        <div class="col-lg-10">
                                            <h6 class="v-event-calendar-list__title"><a href="syndmus.php">Dividendide ja omakapitalist tehtud väljamaksete ning nende saajate deklaratsiooni (vorm INF 1) koos vormi TSD lisaga 7 esitamine</a></h6>
                                        </div>
                                        <div class="col-lg-10 order-lg-last">
                                            <div class="v-event-calendar-list__teaser">
                                                <p>Diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea <a href="javasciprt:;" target="blank">takimata sanctus</a> est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est <a href="javascript:;">Lorem ipsum</a> dolor sit amet.</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="v-event-calendar-list__info">
                                                <p>
                                                    <strong>Toimumise aeg:</strong> 17.02.2018 11:00
                                                </p>
                                                <p>
                                                    <strong>Toimumise koht:</strong> Üle Eesti
                                                </p>
                                                <p>
                                                    <strong>Loe lisaks:</strong>
                                                    <ul class="v-links-list">
                                                        <li class="v-links-list__item"><a href="javascript:;">Mingi link</a></li>
                                                        <li class="v-links-list__item"><a href="javascript:;">Lorem ipsum dolorem</a></li>
                                                        <li class="v-links-list__item"><a href="javascript:;">Ipsum dem amet</a></li>
                                                    </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Fourth item end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php require('components/footer.php'); ?>