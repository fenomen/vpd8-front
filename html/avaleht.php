<?php require('components/header.php'); ?>

<main role="main" class="v-main">

	<section class="v-section v-section--grey">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="card v-main-card">
						<h4>Tuludeklaratsiooni esitama!</h4>
						<p>Füüsilise isiku 2017. aasta tuludeklaratsiooni esitamise tähtaeg on <strong>2. aprill, 2018</strong></p>
						<p><a href="javascript:;" class="btn btn-lg v-btn-primary">Sisene e-MTAsse <i class="vpicon vpicon__arrow-right"></i></a></p>
					</div>
					<div class="card v-main-card">
						<h4>Päevakajaline</h4>
						<ul class="v-links-list">
							<li class="v-links-list__item"><a href="javascript:;">2017. a tulu deklareerimise muudatused</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Maksusoodustuste ülekandmine</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Tulu deklareerimise küsimused ja vastused foorumis</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Maamaksuteated ja maamaksu tasumine</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Kohalike omavalitsuste maakorraldajad</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Maksuvaba tulu alates 01.01.2018</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="card v-main-card">
						<h4>MTA saadab tänasest teavitusi maksuvaba tulu kasutamise kohta</h4>
						<p class="v-main-card__date">19.03.2018</p>
						<p>Maksu- ja tolliamet (MTA) alustab teavituste saatmist inimestele, kellel võib põhjust olla jaanuri ja 
							veebruari töötasude alusel vaadata üle maksuvaba tulu senine kasutamine.</p>
						<p><a href="javascript:;" class="v-link v-link--arrow">Loe lähemalt</a></p>
					</div>
					<div class="card v-main-card">
						<h4>Foorum</h4>
						<p>2017. aasta tulu deklareerimisega seotud küsimusi saate esitada ja vastuseid vaadata <a href="javascript:;"><strong>foorumis</strong></a>.</p>
					</div>
					<div class="card v-main-card">
						<h4>Posti- ja kullerpaki deklareerimine</h4>
						<p><a href="javascript:;" class="v-link v-link--arrow">Uus lihtsustatud deklaratsioon</a></p>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="card v-main-card">
						<h4>Kiirelt kätte</h4>
						<ul class="v-links-list">
							<li class="v-links-list__item"><a href="javascript:;">Rekvisiidid maksude tasumiseks</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Tõendite taotlemine</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Maksuvaba tulu alates 1. jaanuarist 2018</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Erakliendi e-teenuste konto loomine</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Postipaki deklareerimine</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Maksukohustuste ajatamine</a></li>
						</ul>
					</div>
					<div class="card v-main-card">
						<h4>Kontaktid ja ametist</h4>
						<ul class="v-links-list">
							<li class="v-links-list__item"><a href="javascript:;">Teenindusbürood ja tollipunktid</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Vihjeinfo</a></li>
						</ul>
					</div>
					<div class="card v-main-card">
						<h4>Ärikliendile</h4>
						<p>Kui olete äriklient, siis leiate kõik vajaliku <strong><a href="javascript:;">ärikliendi lehelt</a></strong>.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="v-section v-section--light v-section--w-padding">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="v-section__title">Uudised</h2>
				</div>
				<div class="v-news-teaser col-lg-8">
					<div class="v-news-teaser__image">
						<a href="javascript:;">
							<img src="http://via.placeholder.com/300x200" alt="News image" class="img-fluid">
						</a>
					</div>
					<h3 class="v-news-teaser__title"><a href="javascript:;">Aab: Ühtne omavalitsusliit on riigile tugev koostööpartner <small>Lisapealkiri lorem ipsum!</small></a></h3>
					<div class="v-news-teaser__date">27. veebruar 2018 - 11:10</div>
					<p>Täna toimub Eesti Linnade Liidu üldkoosolek, kus tehakse ettepanek moodustada ühine linnade ja valdade liit. Riigihalduse minister Jaak Aab tunnustab omavalitsusi ühise liidu loomisel ning peab seda tugevaks koostööpartneriks.</p>
					<p class="v-news-teaser__read-more"><a href="uudis.php" class="v-link v-link--arrow">Loe lähemalt</a></p>
				</div>
				<div class="col-lg-4">
					<div class="v-news-list v-news-list--front">
						<div class="v-news-list__item">
							<div class="v-news-list__date">27.02.2018</div>
							<div class="v-news-list__title"><a href="uudis.php">Aab: Viljandi on omaette fenomen</a></div>
						</div>
						<div class="v-news-list__item">
							<div class="v-news-list__date">27.02.2018</div>
							<div class="v-news-list__title"><a href="uudis.php">Riigi 2017. aasta tulud ületasid kulusid <small>Lisapealkiri lorem ipsum!</small></a></div>
						</div>
						<div class="v-news-list__item">
							<div class="v-news-list__date">27.02.2018</div>
							<div class="v-news-list__title"><a href="uudis.php">Valitsus kiitis heaks elektriaktsiisi soodustuse energiamahukatele ettevõtetele</a></div>
						</div>
						<div>
							<a href="uudiste-list.php" class="v-link v-link--arrow">Uudiste arhiiv</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<hr class="container" />

	<section class="v-section v-section--light v-section--w-padding">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="v-section__title">Sündmused</h2>
				</div>
				<div class="col">
					<div id="v-event-carousel" class="carousel slide">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<div class="row">
									<div class="col-12 col-lg-4">
										<div class="card v-events-card">
											<div class="v-events-card-date">
												<div class="v-events-card-date__day">2.</div>
												<div class="v-events-card-date__month">aprill</div>
											</div>
											<ul class="v-links-list">
												<li class="v-links-list__item"><a href="syndmus.php">Füüsilise isiku 2017. aasta tuludeklaratsiooni esitamise tähtaeg</a></li>
												<li class="v-links-list__item"><a href="syndmus.php">Maakamksu tasumine</a></li>
											</ul>
										</div>
									</div>
									<div class="col-12 col-lg-4">
										<div class="card v-events-card">
											<div class="v-events-card-date">
												<div class="v-events-card-date__day">16.</div>
												<div class="v-events-card-date__month">aprill</div>
											</div>
											<ul class="v-links-list">
												<li class="v-links-list__item"><a href="syndmus.php">Raskeveokimaksu tasumine</a></li>
											</ul>
										</div>
									</div>
									<div class="col-12 col-lg-4">
										<div class="card v-events-card">
											<div class="v-events-card-date">
												<div class="v-events-card-date__day">2.</div>
												<div class="v-events-card-date__month">juuli</div>
											</div>
											<ul class="v-links-list">
												<li class="v-links-list__item"><a href="syndmus.php">Füüsilise isiku tulumaksu tasumine ja tagastamine füüsilise isiku tuludeklaratsiooni alusel</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<div class="row">
									<div class="col-12 col-lg-4">
										<div class="card v-events-card">
											<div class="v-events-card-date">
												<div class="v-events-card-date__day">16.</div>
												<div class="v-events-card-date__month">aprill</div>
											</div>
											<ul class="v-links-list">
												<li class="v-links-list__item"><a href="syndmus.php">Raskeveokimaksu tasumine</a></li>
											</ul>
										</div>
									</div>
									<div class="col-12 col-lg-4">
										<div class="card v-events-card">
											<div class="v-events-card-date">
												<div class="v-events-card-date__day">2.</div>
												<div class="v-events-card-date__month">juuli</div>
											</div>
											<ul class="v-links-list">
												<li class="v-links-list__item"><a href="syndmus.php">Füüsilise isiku tulumaksu tasumine ja tagastamine füüsilise isiku tuludeklaratsiooni alusel</a></li>
											</ul>
										</div>
									</div>
									<div class="col-12 col-lg-4">
										<div class="card v-events-card">
											<div class="v-events-card-date">
												<div class="v-events-card-date__day">2.</div>
												<div class="v-events-card-date__month">aprill</div>
											</div>
											<ul class="v-links-list">
												<li class="v-links-list__item"><a href="syndmus.php">Füüsilise isiku 2017. aasta tuludeklaratsiooni esitamise tähtaeg</a></li>
												<li class="v-links-list__item"><a href="syndmus.php">Maakamksu tasumine</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a class="carousel-control-prev" href="#v-event-carousel" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#v-event-carousel" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<div class="col-sm-12">
					<a href="syndmuste-kalender.php" class="v-link v-link--arrow v-link--arrow-down">Näita kõiki</a>
				</div>
			</div>
		</div>
	</section>

	<section class="v-section v-section--grey v-section--front">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="card v-main-card">
						<h4>Päringud</h4>
						<ul class="v-links-list">
							<li class="v-links-list__item"><a href="javascript:;">Personaalse viitenumbri otsing</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Maksuvõla kontroll</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Eesti Tollitariifistik (ETT)</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Kingituste ja annetuste ning muude tulude kasutamise kontroll alates 2015. a</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Mitteresidentsuse kontrollimine</a></li>
							<li class="v-links-list__item"><a href="javascript:;">Alkoholi maksumärgi avalik päring</a></li>
						</ul>
					</div>
					<div class="card v-main-card">
						<h4>Ümbrikuga tervist ei paranda</h4>
						<p>Ümbrikupalka vastu võttes jääd ilma olulistest hüvedest.</p>
						<p><a href="javascript:;" class="v-link v-link--arrow">Uus lihtsustatud deklaratsioon</a></p>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="card v-main-card">
						<h4>Veebiseminar "Internetikaubanduse võlud ja valud"</h4>
						<p>6. detsembril 2017 toimunud veebinari õppematerjal ja salvestus.</p>
						<p><a href="javascript:;" class="v-link v-link--arrow">Vaata lähemalt</a></p>
					</div>
					<div class="card v-main-card">
						<h4>Veebinar "Üksikisiku maksuvaba tulu 2018. aastal"</h4>
						<p>Alates 2018. a sõltub maksuvaba tulu suurus Teie aastasest tulust. Planeerige tulu suurust juba aasta alguses, et vältida 2019. aastal tulumaksu juurde tasumist.</p>
						<p><a href="javascript:;" class="v-link v-link--arrow">Vaata lähemalt</a></p>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="card v-main-card">
						<h4>Infotelefonid</h4>
						<div class="v-info-combo">
							<p class="v-info-combo__lead">880 0811</p>
							<p>Eraklientide ja FIEde nõustamine</p>
							<p><a href="javascript:;">eraklient@emta.ee</a></p>
						</div>
						<div class="v-info-combo">
							<p class="v-info-combo__lead">880 0812</p>
							<p>Äriklientide nõustamine ja käibemaksuinfo</p>
							<p><a href="javascript:;">eraklient@emta.ee</a></p>
						</div>
						<div class="v-info-combo">
							<p class="v-info-combo__lead">880 0814</p>
							<p>Tolliinfo, e-toll</p>
							<p><a href="javascript:;">eraklient@emta.ee</a></p>
						</div>
						<div class="v-info-combo">
							<p class="v-info-combo__lead">880 0815</p>
							<p>E-maksuamet</p>
							<p><a href="javascript:;">eraklient@emta.ee</a></p>
						</div>
						<div class="v-info-combo">
							<p class="v-info-combo__lead">880 0822</p>
							<p>Üldinfo, võlainfo, maamaks, raskeveokimaks</p>
							<p>Skype: <a href="javascript:;">mta.eesti</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php require('components/footer.php'); ?>