<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 mt-5 mb-5">

                    <h1 class="page-title">
                        404 - Lehekülge ei leitud
                    </h1>

                    <div class="v-">
                        <p>Otsitud lehekülge ei leitud.</p>
                        <p><a href="javascript:history.back(-1);" class="btn btn-primary v-btn-primary">Mine tagasi</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>