<?php require("components/header.php"); ?>

<main role="main" class="v-main">
    <section class="v-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb v-breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Avaleht</a></li>
                            <li class="breadcrumb-item"><a href="uudiste-list.php">Uudised</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ümbrikupalga saajate arv...</li>
                        </ol>
                    </nav>

                    <h1 class="page-title">
                        Lehe tiitel
                    </h1>

                    <div class="v-">

                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php require("components/footer.php"); ?>