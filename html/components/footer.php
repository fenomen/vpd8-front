            <footer class="v-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <ul class="v-footer__info">
                                <li>Maksu- ja Tolliamet</li>
                                <li>Lõõtsa 8a, 15176 Tallinn</li>
                                <li>Telefon: <a href="tel:880 0810">880 0810</a></li>
                                <li>E-post: <a href="mailto:emta@emta.ee">emta@emta.ee</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <ul class="v-footer__links-list">
                                <li class="v-footer__links-list__item"><i class="fab fa-facebook-f"></i><a href="javascript">Facebook</a></li>
                                <li class="v-footer__links-list__item"><i class="fab fa-twitter"></i><a href="javascript">Twitter</a></li>
                                <li class="v-footer__links-list__item"><i class="fab fa-youtube"></i><a href="javascript">Videod</a></li>
                            </ul>
                            <ul class="v-footer__links-list">
                                <li class="v-footer__links-list__item"><i class="fas fa-rss"></i><a href="javascript">RSS voog</a></li>
                                <li class="v-footer__links-list__item"><i class="fas fa-comments"></i><a href="javascript">Foorum</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="v-footer-cta">
                                <a href="javascript:;" class="btn btn-lg btn-outline v-footer-cta__button v-footer-cta__button--outline">Välisametnik <i class="vpicon vpicon__arrow-right"></i></a>
                                <p>
                                    <a href="javascript:;" class="v-link">Sisukaart</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="v-scroll-to-top"><a href="#top"><i class="fas fa-angle-up fa-2x"></i></a></div>
            </footer>
        </div> <?php /* End site-wrapper */ ?>
        <script type='text/javascript' src='assets/js/vendor.js'></script>
        <script type='text/javascript' src='assets/js/main.js'></script>
    </body>
</html>
