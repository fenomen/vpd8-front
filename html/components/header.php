<!DOCTYPE html>
<html lang="et">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.5"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="Valitsusportaal">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">

		<title>Valitsusportaal</title>
		<link rel="shortcut icon" type="image/ico" href="../favicon.ico">
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
		<link rel='stylesheet' href='assets/css/main.css' type='text/css' media='all'>
	</head>
	<body>
		<header>
			<nav class="v-top-nav">
				<div class="v-top-nav__mobile">
					<div class="container">
						<div class="row">
							<div class="col">
								<a href="javascript:;" class="v-top-nav__collapse-button" data-toggle="collapse" data-target=".v-top-nav__menu" aria-controls="v-top-nav__menu" aria-expanded="false" aria-label="Toggle navigation">
									<i class="vpicon vpicon__down"></i> Eraklient
								</a>
							</div>
							<div class="col-auto">
								<a href="javascript:;" class="v-top-nav__collapse-button" data-toggle="collapse" data-target=".v-lang-switcher" aria-controls="v-lang-switcher" aria-expanded="false" aria-label="Toggle navigation">
									<i class="vpicon vpicon__down"></i> EST
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col">
							<ul class="v-top-nav__menu d-lg-block collapse">
								<li class="v-top-nav__menu_link v-top-nav__menu_link--active"><a href="javascript:;">Eraklient</a></li>
								<li class="v-top-nav__menu_link"><a href="javascript:;">Äriklient</a></li>
								<li class="v-top-nav__menu_link"><a href="javascript:;">Kontaktid ja ametist</a></li>
							</ul>
						</div>
						<div class="col-auto">
							<ul class="v-lang-switcher d-lg-block collapse">
								<li class="v-lang-switcher__item v-lang-switcher__item--active"><a href="javascript:;">EST</a></li>
								<li class="v-lang-switcher__item"><a href="javascript:;">RUS</a></li>
								<li class="v-lang-switcher__item"><a href="javascript:;">ENG</a></li>
							</ul>
						</div>
						<div class="col-auto d-none d-lg-block">
							<a href="javascript:;" class="v-top-nav__accessibility"><i class="vpicon vpicon__accessibility"></i></a>
						</div>
					</div>
				</div>
			</nav>
			<div class="v-header-middle">
				<div class="container">
					<div class="v-header-middle__wrap">
						<div class="row align-items-center">
							<div class="col">
								<div class="v-logo">
									<a href="avaleht.php">
										<img src="img/vpd8_logo.png" alt="" class="v-logo__img">
									</a>
								</div>
							</div>
							<div class="col-auto d-lg-none">
								<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#searchCollapse" aria-controls="searchCollapse" aria-expanded="false" aria-label="Toggle navigation">
									<i class="vpicon vpicon__search"></i>
								</button>
								<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
									<i class="vpicon vpicon__hamburger"></i>
								</button>
							</div>
							<div class="col-12 col-lg-4 v-header-search d-none d-lg-block">
								<form action="otsing.php">
									<input type="text" placeholder="Otsi..." class="v-header-search__input" />
									<button type="submit" class="v-header-search__submit"></button>
								</form>
							</div>
							<div class="col d-none d-lg-block text-right">
								<a href="javascript:;" class="btn v-btn-primary btn-lg">Sisene e-MTAsse <i class="vpicon vpicon__arrow-right"></i></a>
							</div>
	
						</div>
					</div>
				</div>				
			</div>
			<!--Mob search-->
			<div class="collapse search-collapse" id="searchCollapse">
				<div class="v-header-search v-header-search--mobile">
					<form action="otsing.php">
						<input type="text" placeholder="Otsi..." class="v-header-search__input" />
						<button type="submit"></button>
					</form>
				</div>
			</div>
			<!--Mob search end-->
			<!--Nav-->
			<div class="collapse navbar-collapse d-lg-block" id="navbarCollapse">
				<div class="container">
					<ul class="nav navbar-nav v-site-nav">
						<li class="v-site-nav__login-btn d-lg-none text-center">
							<a href="javascript:;" class="btn btn-lg v-btn-primary v-btn-primary--outline">Sisene e-MTAsse <i class="vpicon vpicon__arrow-right"></i></a>
						</li>
						<li class="v-site-nav__item">
							<a href="javascript:;" class="v-site-nav__link">Tulu<br>deklareerimine</a>
							<span class="v-site-nav__sub-menu-toggle"><i class="vpicon vpicon__down"></i></span>
							<div class="v-site-nav__sub-menu container animated fadeInLeft">
								<div class="row">
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Tulu deklareerimine</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018 aasta tulude deklareerimisel</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Tulumaksu tagastamine ja juurdemakse</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Maksumäärad</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Deklaratsioonide vormid ja nende täitmise juhised</a></li>
										</ul>
									</div>
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Maksustatavad tulud</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Palgatulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Pensionid ja kindlustushüvitised</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Ettevõtlustulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Elukoha müügi maksuvastus</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Vara võõrandamine</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Tulu finantsvaralt</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Intress</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Kasvava metsa raieõigus ja metsamaterjal</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Üüri- ja renditulu, litsentsitasu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Stipendiumid, toetused ja hüvitised</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Välisriigis saadud tulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Internetis kaupade tellimine ja müümine</a></li>											
											<li class="v-links-list__item"><a href="javascript:;">Muu tulu</a></li>
										</ul>
									</div>
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Maksusoodustused</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Üldiselt maksusoodustustest</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Maksuvaba tulu 2017. aastal</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Eluasemelaenu intressid</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Koolituskulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Kingitused ja annetused</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Maksusoodustuste ülekandmine</a></li>
										</ul>
										<h6 class="v-site-nav__sub-menu-title">Maksusoodustused</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Maksuvaba tulu alates 1. jaanuarist 2018</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Oluline maksuvaba tulu arvestamisel alates 1. jaanuarist 2018</a></li>
										</ul>
									</div>
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Eraisikust tööandjale</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Eraisikust tööandjale</a></li>
										</ul>
										<h6 class="v-site-nav__sub-menu-title">Füüsilisest isikust ettevõtjale</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Füüsilisest isikust ettevõtja maksustamise muudatused 2018. aastal</a></li>
										</ul>
										<h6 class="v-site-nav__sub-menu-title">Mitteresidendile</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Mitteresidendi maksustamisest</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Mahaarvestamised Eestis maksustatavast tulust</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Tulumaksu tagastamine</a></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
						<li class="v-site-nav__item">
							<a href="javascript:;" class="v-site-nav__link">Maa, mets, sõiduk,<br> kütus, hasartmäng</a>
							<a href="javascript:;" class="v-site-nav__sub-menu-toggle"><i class="vpicon vpicon__down"></i></a>
							<div class="v-site-nav__sub-menu container animated fadeInLeft">
								<div class="row">
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Maksustatavad tulud</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Palgatulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Pensionid ja kindlustushüvitised</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Ettevõtlustulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Elukoha müügi maksuvastus</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Vara võõrandamine</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Tulu finantsvaralt</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Intress</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Kasvava metsa raieõigus ja metsamaterjal</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Üüri- ja renditulu, litsentsitasu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Stipendiumid, toetused ja hüvitised</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Välisriigis saadud tulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Internetis kaupade tellimine ja müümine</a></li>											
											<li class="v-links-list__item"><a href="javascript:;">Muu tulu</a></li>
										</ul>
									</div>
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Tulu deklareerimine</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">2017. aasta tuludeklaratsiooni esitamine</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Olulisemad muudatused 2017. aasta ja 2018 aasta tulude deklareerimisel</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Tulumaksu tagastamine ja juurdemakse</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Maksumäärad</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Deklaratsioonide vormid ja nende täitmise juhised</a></li>
										</ul>
									</div>
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Eraisikust tööandjale</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Eraisikust tööandjale</a></li>
										</ul>
										<h6 class="v-site-nav__sub-menu-title">Füüsilisest isikust ettevõtjale</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Füüsilisest isikust ettevõtja maksustamise muudatused 2018. aastal</a></li>
										</ul>
										<h6 class="v-site-nav__sub-menu-title">Mitteresidendile</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Mitteresidendi maksustamisest</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Mahaarvestamised Eestis maksustatavast tulust</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Tulumaksu tagastamine</a></li>
										</ul>
									</div>
									<div class="col-lg-3">
										<h6 class="v-site-nav__sub-menu-title">Maksusoodustused</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Üldiselt maksusoodustustest</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Maksuvaba tulu 2017. aastal</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Eluasemelaenu intressid</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Koolituskulu</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Kingitused ja annetused</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Maksusoodustuste ülekandmine</a></li>
										</ul>
										<h6 class="v-site-nav__sub-menu-title">Maksusoodustused</h6>
										<ul class="v-links-list">
											<li class="v-links-list__item"><a href="javascript:;">Maksuvaba tulu alates 1. jaanuarist 2018</a></li>
											<li class="v-links-list__item"><a href="javascript:;">Oluline maksuvaba tulu arvestamisel alates 1. jaanuarist 2018</a></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
						<li class="v-site-nav__item">
							<a href="javascript:;" class="v-site-nav__link">Reisimine, saadetised,<br> ümberasumine</a>
							<a href="javascript:;" class="v-site-nav__sub-menu-toggle"><i class="vpicon vpicon__down"></i></a>
							<div class="v-site-nav__sub-menu container animated fadeInLeft">

							</div>
						</li>
						<li class="v-site-nav__item">
							<a href="javascript:;" class="v-site-nav__link">Maksude ja nõute<br> tasumine, võlad</a>
							<a href="javascript:;" class="v-site-nav__sub-menu-toggle"><i class="vpicon vpicon__down"></i></a>
							<div class="v-site-nav__sub-menu container animated fadeInLeft">

							</div>
						</li>
						<li class="v-site-nav__item">
							<a href="javascript:;" class="v-site-nav__link">Maksukorraldus,<br> maksusüsteem</a>
						</li>
					</ul>
				</div>
			</div>
			<!--Nav end-->

			<!-- #top is for navigating back to top, via #v-scroll-to-top button -->
			<div id="top"></div>
		</header>
