(function ($) {
  $(window).on('scroll', function() {
    var scrollTop = $(window).scrollTop(),
      screenHeight = $(window).height();

    if (scrollTop > screenHeight) {
      $('#v-scroll-to-top').addClass('reveal');
    } else {
      $('#v-scroll-to-top').removeClass('reveal');
    }
  });
})(jQuery);



