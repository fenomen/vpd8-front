(function ($) {
  $('.v-site-nav__item').on('click', function(e) {
    // if nav item is NOT active
    if (!$(this).hasClass('v-site-nav__item--active')) {
        // Add active class
        $(this).toggleClass('v-site-nav__item--active');
        // Toggle menu icon
        $('.v-site-nav__item--active .v-site-nav__sub-menu-toggle i.vpicon__up').removeClass('vpicon__up').addClass('vpicon__down');
        $('.v-site-nav__sub-menu-toggle i', $(this)).toggleClass('vpicon__up').toggleClass('vpicon__down');
        // Add overlay to body
        $('main[role="main"]').addClass('js-has-overlay');
        // Close other --active nav items.
        $('.v-site-nav__item--active').not($(this)).removeClass('v-site-nav__item--active');
    } else {
        $('.v-site-nav__item--active').removeClass('v-site-nav__item--active');
        $('.v-site-nav__sub-menu-toggle i', $(this)).toggleClass('vpicon__up').toggleClass('vpicon__down');
        $('main[role="main"]').removeClass('js-has-overlay');
    }
  });
})(jQuery);



// var $menu = $('.v-site-nav__item--active');

// $(document).mouseup(function (e) {
//     if (!$menu.is(e.target) // if the target of the click isn't the container...
//     && $menu.has(e.target).length === 0) {
//         $('.v-site-nav__item--active').removeClass('v-site-nav__item--active');
//         $('.v-site-nav__link--active').removeClass('v-site-nav__link--active');
//     }
//  });