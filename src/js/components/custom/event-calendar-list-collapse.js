(function ($) {
$('.v-event-calendar-list__date').on('click', function() {
    $('i', $(this)).toggleClass('vpicon__down').toggleClass('vpicon__up'); // toggle icon
    $(this).closest('.v-event-calendar-list__item').toggleClass('v-event-calendar-list__item--open');
  });
})(jQuery);
