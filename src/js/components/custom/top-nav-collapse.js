(function ($) {
  $('.v-top-nav__collapse-button').on('click', function() {
    $('i', $(this)).toggleClass('vpicon__down').toggleClass('vpicon__up');
  });
})(jQuery);
