(function ($) {
  if ($('.v-accordion').length) {
    // Opening accordion based on URL
    var url = document.location.toString();
    if (url.match('#')) {
      $('#' + url.split('#')[1]).addClass('show');
      $('a[href="#' + url.split('#')[1] + '"]').removeClass('collapsed');

      $('html, body').animate({
        scrollTop: $($('a[href="#' + url.split('#')[1] + '"]')).offset().top
      }, 300);
    }
  }
})(jQuery);
