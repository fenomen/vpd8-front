(function ($) {
  $('.datepicker').datepicker({
    format: 'dd.mm.yyyy',
    weekStart: 1,
  });
})(jQuery);

