const gulp = require('gulp');
const concat = require('gulp-concat');
const gulpif = require('gulp-if');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const livereload = require('gulp-livereload');
const gutil = require('gulp-util');
const uglify = require('gulp-uglify');

var config = require('./gulp-config.json');

config.base = {
	SRC: './src/',
	DIST: './html/'
};

config.src = {
	SASS: {
		MAIN: [
			config.base.SRC + 'sass/**/*.scss'
		]
	},
	JS: {
		VENDOR: [
			config.base.SRC + 'js/vendor/*.js'
		],
		MAIN: [
			config.base.SRC + 'js/library.js',
			config.base.SRC + 'js/components/**/*.js'
		]
	}
};

config.dist = {
	ASSETS: config.base.DIST + 'assets/',
	SASS: config.base.DIST + 'assets/',
	JS: config.base.DIST + 'assets/'
};

config.environment = gutil.env.prod ? 'prod' : 'dev';
config.compress = config.environment === 'dev';

gulp.task('css:main', function() {
	return gulp.src([].concat(config.src.SASS.MAIN))
		.pipe(gulpif(!config.compress, sourcemaps.init()))
		.pipe(sassGlob())
		.pipe(sass({ outputStyle: config.compress ? 'compressed' : 'normal' }).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(concat('css/main.css'))
		.pipe(gulpif(!config.compress, sourcemaps.write()))
		.pipe(gulp.dest(config.dist.SASS));
});

gulp.task('js:vendor', function() {
	return gulp.src([].concat(config.src.JS.VENDOR))
		.pipe(gulpif(!config.compress, sourcemaps.init()))
		.pipe(gulpif(config.compress, uglify()))
		.pipe(concat('js/vendor.js'))
		.pipe(gulpif(!config.compress, sourcemaps.write()))
		.pipe(gulp.dest(config.dist.ASSETS));
});

gulp.task('js:main', function() {
	return gulp.src(config.src.JS.MAIN)
		.pipe(gulpif(!config.compress, sourcemaps.init()))
		.pipe(gulpif(config.compress, uglify()))
		.pipe(concat('js/main.js'))
		.pipe(gulpif(!config.compress, sourcemaps.write()))
		.pipe(gulp.dest(config.dist.ASSETS));
});

//Watch task
gulp.task('watch', ['build'], function() {
	livereload.listen();

	gulp.watch(config.src.SASS.MAIN, ['css:main']);
	gulp.watch(config.src.JS.VENDOR, ['js:vendor']);
	gulp.watch(config.src.JS.MAIN, ['js:main']);

	gulp.watch(config.base.DIST + '/**/*', function(obj) {
		livereload.changed(obj.path);
	});
});

gulp.task('build', ['css:main', 'js:vendor', 'js:main']);
gulp.task('default', ['watch']);
